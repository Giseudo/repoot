<?php namespace Qualitare\Repoot\Components;

use Cms\Classes\ComponentBase;
use ApplicationException;

class Toggle extends ComponentBase
{

	public function componentDetails()
	{
		return [
			'name'        => 'Toggle',
			'description' => 'Componente para campo booleano.'
		];
	}

	public function defineProperties()
	{
		return [
			//
		];
	}

	public function onRun()
	{
		//
	}
}
