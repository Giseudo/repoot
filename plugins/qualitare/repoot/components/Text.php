<?php namespace Qualitare\Repoot\Components;

use Cms\Classes\ComponentBase;
use ApplicationException;

class Text extends ComponentBase
{

	public function componentDetails()
	{
		return [
			'name'        => 'Text',
			'description' => 'Componente para campo de texto.'
		];
	}

	public function defineProperties()
	{
		return [
			'value' => [
				'title' => 'Field value',
				'default' => '',
				'type' => 'string'
			],
			'name' => [
				'title' => 'Field name',
				'default' => 'text',
				'type' => 'string'
			],
			'label' => [
				'title' => 'Field label',
				'default' => '',
				'type' => 'string'
			],
			'type' => [
				'title' => 'Field type',
				'default' => 'text',
				'type' => 'string'
			]
		];
	}

	public function onRun()
	{
		//
	}
}
