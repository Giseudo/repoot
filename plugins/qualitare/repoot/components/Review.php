<?php namespace Qualitare\Repoot\Components;

use Cms\Classes\ComponentBase;
use ApplicationException;

class Review extends ComponentBase
{

	public function componentDetails()
	{
		return [
			'name'        => 'Review',
			'description' => 'Componente de avaliação.'
		];
	}

	public function defineProperties()
	{
		return [
            'value' => [
                'title' => 'Map',
                'description' => 'The most amount of todo items allowed',
                'default' => [
                    'reviews' => [
                                    ['name' => 'Bruna', 'score' => 5, 'type' => 'negative', 'title' => 'Teste de title', 'date' => '5 de Agosto de 2018', 'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'],
                                    ['name' => 'Tereza', 'score' => 9, 'type' => 'positive', 'title' => 'Teste de title2', 'date' => '10 de Fevereiro de 2018', 'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'],
                                    ['name' => 'Marcio', 'score' => 10, 'type' => 'positive', 'title' => 'Teste de title3', 'date' => '3 de Maio de 2018', 'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'],
                                    ['name' => 'Matheus', 'score' => 7, 'type' => 'neutral', 'title' => 'Teste de title4', 'date' => '16 de Junho de 2018', 'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'],
                                    ['name' => 'Cristina', 'score' => 6, 'type' => 'neutral', 'title' => 'Teste de title5', 'date' => '25 de Junho de 2018', 'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'],
                                    ['name' => 'Caio', 'score' => 2, 'type' => 'negative', 'title' => 'Teste de title6', 'date' => '12 de Janeiro de 2018', 'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'],
                                ]
                ],
                'type' => 'string',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'The Max Items property can contain only numeric symbols'
            ]
		];
	}

	public function onRun()
	{
		//
	}
}
