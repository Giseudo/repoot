<?php namespace Qualitare\Repoot\Components;

use Cms\Classes\ComponentBase;
use ApplicationException;

class Score extends ComponentBase
{

	public function componentDetails()
	{
		return [
			'name'        => 'Score',
			'description' => 'Componente que apresenta a reputação.'
		];
	}

	public function defineProperties()
    {
        return [
            'value' => [
                 'title'             => 'Score Reputation',
                 'description'       => 'The most amount of todo items allowed',
                 'default'           => 5,
                 'type'              => 'string',
                 'validationPattern' => '^[0-9]+$',
                 'validationMessage' => 'The Max Items property can contain only numeric symbols'
            ]
        ];
    }
    
    
    
}
