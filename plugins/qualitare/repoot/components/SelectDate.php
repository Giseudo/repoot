<?php namespace Qualitare\Repoot\Components;

use Cms\Classes\ComponentBase;
use ApplicationException;

class SelectDate extends ComponentBase
{
	public function componentDetails()
	{
		return [
			'name'        => 'Select Date',
			'description' => 'Componente para campo de data.'
		];
	}

	public function defineProperties()
	{
		return [
			'size' => [
				'title' => 'Component size',
				'default' => 'md',
				'type' => 'string'
			],
			'selected' => [
				'title' => 'The selected items',
				'default' => [7 => true],
				'type' => 'array'
			],
			'options' => [
				'title' => 'The options',
				'default' => [
					0 => 'Hoje',
					1 => 'Ontem',
					7 => 'Últimos 7 Dias',
					30 => 'Últimos 30 Dias',
					90 => 'Últimos 90 Dias'
				],
				'type' => 'array'
			]
		];
	}

	public function onRun()
	{
		//
	}
}
