import './style.scss'

$$('.rp-select').each(function() {
	let $el = $$(this),
		$dropdown = $el.find('.rp-select__dropdown'),
		$options = $el.find('.rp-select__option'),
		$label = $el.find('.rp-select__label'),
		$value = $el.find('.rp-select__value'),
		multiple = $el.data('multiple') == 1,
		selected = []

	// Update select component elements
	this.update = function() {
		// Reset selected array
		selected = []

		// Populate selected array
		$options.each(function() {
			let $option = $$(this),
				$checkbox = $option.find('input[type="checkbox"]'),
				text = $option.find('span').html()

			if ($checkbox.prop('checked'))
				selected.push(text)
		})

		// Replace multiple whitespaces
		selected = selected.map(text => text.trim())

		// Update value text
		if (selected.length > 0)
			$value.html(selected.join(', '))
		else
			$value.html('Selecione uma opção')
	}

	// Dropdown option events
	$options.each(function() {
		let $option = $$(this)

		// On option click
		$option.on('click', event => {
			let $checkbox = $option.find('input[type="checkbox"]'),
				$checkboxes = $dropdown.find('input[type="checkbox"]')

			// Only if select has multiple support
			if (!multiple) {
				$checkboxes.prop('checked', false)
				$checkbox.prop('checked', true)
			}

			// Update label text
			$el[0].update()
		})
	})

	// Add is-focus class on element focus
	$el.on('focus', event => $el.addClass('is-focus'))

	// Remove is-focus class on element blur
	$el.on('blur', event => $el.removeClass('is-focus'))

	// Prevent from dropdown closing on child click
	$dropdown.on('click', event => $el.focus())

	// Update component on start
	// this.update()
})
