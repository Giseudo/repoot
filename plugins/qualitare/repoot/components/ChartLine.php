<?php

namespace Qualitare\Repoot\Components;

use Cms\Classes\ComponentBase;
use ApplicationException;

class ChartLine extends ComponentBase {
	public function init()
	{
		$this->addComponent('\Qualitare\Repoot\Components\SelectDate', 'selectdate', [
			//
		]);
	}

	public function componentDetails() {
		return [
			'name' => 'ChartLine',
			'description' => 'Componente de gráfico interativo.'
		];
	}

	public function defineProperties() { 
		return [
			'value' => [
				'title' => 'ChartLine Reputation',
				'description' => 'The most amount of todo items allowed',
				'default' => [
					'labels' => ['1', '2', '3', '4', '5', '6', '7'],
					'datasets' => [
						[
							'fill' => 'false',
							'label' => 'Positivo',
							'borderColor' => 'rgb(51, 204, 51)',
							'data' => [0.4, 1.2, 4, 3.6, 9, 8, 3.2]
						],
						[
							'fill' => 'false',
							'label' => 'Neutro',
							'borderColor' => 'rgb(255, 255, 102)',
							'data' => [2.2, 5.3, 1, 1.5, 8, 1.7, 9.3]
						],
						[
							'fill' => 'false',
							'label' => 'Negativo',
							'borderColor' => 'rgb(255, 0, 0)',
							'data' => [4.7, 8.3, 5.2, 7, 10, 3.2, 4.6]
						],
					]
				],
				'type' => 'string',
				'validationPattern' => '^[0-9]+$',
				'validationMessage' => 'The Max Items property can contain only numeric symbols'
			]
		];
	}

	function onRun() {
		//
	}

}

