import './style.scss'

$$('.rp-number').each(function() {
	let $el = $$(this),
		$input = $el.find('.rp-number__input'),
		$increase = $el.find('.rp-number__increase'),
		$decrease = $el.find('.rp-number__decrease')

	this.step = 1
	this.max = 999
	this.min = 0

	this.increase = (e) => {
		let value = +$input.val(),
			quant = value < this.max || this.max == 0
				? value + this.step
				: this.max

		$input.val(quant)
		e.preventDefault()
	}

	this.decrease = (e) => {
		let value = +$input.val(),
			quant = value > this.min
				? value - this.step
				: this.min

		$input.val(quant)
		e.preventDefault()
	}

	$increase.on('click', e => this.increase(e))
	$decrease.on('click', e => this.decrease(e))

	$input.on('keydown', e => {
		// Up key: Increase the value
		if (e.keyCode === 38) {
			this.increase()
			e.preventDefault()
			return
		}

		// Down key: Decrease the value
		if (e.keyCode === 40) {
			this.decrease()
			e.preventDefault()
			return
		}

		// Allow these keys only:
		if (
			// backspace, delete, tab, escape, enter, dot
			[46, 8, 9, 27, 13, 110, 190].indexOf(e.keyCode) >= 0 ||
			// Ctrl/cmd+A
			(e.keyCode === 65 && (e.ctrlKey || e.metaKey)) ||
			// Ctrl/cmd+C
			(e.keyCode === 67 && (e.ctrlKey || e.metaKey)) ||
			// Ctrl/cmd+R
			(e.keyCode === 82 && (e.ctrlKey || e.metaKey)) ||
			// Ctrl/cmd+X
			(e.keyCode === 88 && (e.ctrlKey || e.metaKey)) ||
			// home, end, left, right
			(e.keyCode >= 35 && e.keyCode <= 39)
		) {
			return
		}

		if (
			(e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) &&
			(e.keyCode < 96 || e.keyCode > 105)
		) {
			e.preventDefault()
		}
	})
})
