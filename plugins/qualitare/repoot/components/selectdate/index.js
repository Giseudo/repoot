import './style.scss'
import 'imports-loader?$=jquery,moment!jquery-date-range-picker/dist/jquery.daterangepicker.min.js'
import moment from 'moment'

$$('.rp-select--date').each(function() {
	let $el = $(this),
		$input = $el.find('.rp-select__input'),
		$cancel = $el.find('.rp-select__cancel'),
		$apply = $el.find('.rp-select__apply'),
		$options = $el.find('input[type="checkbox"]')

	$input.dateRangePicker({
		inline: true,
		container: $el.find('.rp-select__container'),
		alwaysOpen: true,
		singleMonth: true,
		showShortcuts: false,
		monthSelect: true,
		yearSelect: true
	})

	$el.on('change', event => {
		let value = event.target.value,
			today = moment().format('YYYY-MM-DD')

		if (value == 'custom') {
			$el.addClass('is-range')
		} else if (value == '30') {
			let formated = moment().add(-1, 'months').format('YYYY-MM-DD')
			$input.val(`${formated} – ${today}`)
		} else {
			let formated = moment().add(+value * -1, 'days').format('YYYY-MM-DD')
			if (value == '0') {
				$input.val(`${formated}`)
			} else {
				$input.val(`${formated} – ${today}`)
			}
			$el.removeClass('is-range')
		}
	})

	$cancel.on('click', event => {
		$options.prop('checked', false)
		$el.removeClass('is-range')
		$el.focus()
		event.preventDefault()
	})

	$apply.on('click', event => {
		$el.removeClass('is-focus')
		event.preventDefault()
	})
})
