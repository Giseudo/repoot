<?php namespace Qualitare\Repoot\Components;

use Cms\Classes\ComponentBase;
use ApplicationException;

class ChartDoughnut extends ComponentBase
{

	public function componentDetails()
	{
		return [
			'name'        => 'ChartLine',
			'description' => 'Componente de gráfico interativo.'
		];
	}

	public function defineProperties()
	{
		return [
                'value' => [
                    'title' => 'Doughnut Reputation',
                    'description' => 'The most amount of todo items allowed',
                    'default' => [],
                    'type' => 'string',
                    'validationPattern' => '^[0-9]+$',
                    'validationMessage' => 'The Max Items property can contain only numeric symbols'
                ]
            ];
	}

	public function onRun()
	{
		//
	}
}
