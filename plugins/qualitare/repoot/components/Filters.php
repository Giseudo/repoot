<?php namespace Qualitare\Repoot\Components;

use Cms\Classes\ComponentBase;
use ApplicationException;

class Filters extends ComponentBase
{

	public function componentDetails()
	{
		return [
			'name'        => 'Filters',
			'description' => 'Componente de filtros.'
		];
	}

	public function defineProperties()
	{
		return [
			//
		];
	}

	public function onRun()
	{
		//
	}
}
