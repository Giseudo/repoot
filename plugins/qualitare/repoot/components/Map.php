<?php

namespace Qualitare\Repoot\Components;

use Cms\Classes\ComponentBase;
use ApplicationException;

class Map extends ComponentBase {

    public function componentDetails() {
        return [
            'name' => 'Map',
            'description' => 'Componente de mapa.'
        ];
    }

    public function defineProperties() {
        return [
            'markers' => [
                'title' => 'Markers',
                'description' => '',
                'default' => [],
                'type' => 'array',
            ],
            'center' => [
                'title' => 'Markers',
                'description' => '',
                'default' => [
                    'lat' => 0,
                    'lng' => 0
                ],
                'type' => 'array',
            ]
        ];
    }

    public function onRun() {
        //
    }

}
