import './style.scss'

$$('.rp-filters').each(function() {
	let $el = $$(this),
		$add = $el.find('.rp-filters__add'),
		$options = $el.find('.rp-filters__option')

	$options.each(function() {
		let $option = $$(this)

		$option.on('click', event => {
			// TODO: Show/hide filter fields on form
			alert(`TODO: Add filter type ${$option.data('value')}. \n repoot/plugins/qualitare/components/filters/index.js`)

			event.preventDefault()
		})
	})

	$add.on('click', event => {
		$el.addClass('is-focus')
		event.preventDefault()
	})

	$add.on('blur', event => $el.removeClass('is-focus'))
})
