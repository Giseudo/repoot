import './style.scss'
import Chart from 'chart.js'

$$('.rp-chart-doughnut').each(function() {
	let $el = $$(this),
		$canvas = $el.find('.rp-chart-doughnut__canvas'),
		dataChart = JSON.parse($el.data('value'))

	// TODO Scale on hover 
	/* dataChart.datasets.forEach(dataset => {
		dataset.hoverBackgroundColor = []
		dataset.backgroundColor.forEach((background, index) => {
			console.log(background)
			dataset.backgroundColor[index] = createGrad($canvas[0], background)
			dataset.hoverBackgroundColor[index] = background
			dataset.borderWidth = 1
			dataset.borderColor = '#fff'
			dataset.hoverBorderColor = '#fff'
		})
	}) */

	this.chart = new Chart($canvas, {
		type: 'doughnut',
		data: dataChart,
		options: {
			cutoutPercentage: 70,
			animation: {
				animateScale: true
			},
			legend: {
				display: false
			}
		}
	})
})

function hexToRgb(hex) {
	var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
	return result ? {
		r: parseInt(result[1], 16),
		g: parseInt(result[2], 16),
		b: parseInt(result[3], 16)
	} : null;
}

function createGrad(ctx, colorOne = "#62D7FA", opacityOne = 1, colorTwo = "#ffffff", opacityTwo = 1, colorStop = 95){
	var rgbOne = hexToRgb(colorOne);	
	var rgbTwo = hexToRgb(colorTwo);
	var canvasWidth = $$(ctx).width();
	colorStop = colorStop/100;

	var grd = ctx.getContext("2d").createRadialGradient(
		canvasWidth/2, 
		canvasWidth/2, 
		0.000, 
		canvasWidth/2, 
		canvasWidth/2, 
		canvasWidth/2
	);

	// Add colors
	grd.addColorStop(0.000, 'rgba(' + rgbOne.r + ', ' + rgbOne.g + ', ' + rgbOne.b + ', ' + opacityOne + ')');
	grd.addColorStop(colorStop, 'rgba(' + rgbOne.r + ', ' + rgbOne.g + ', ' + rgbOne.b + ', ' + opacityOne + ')');
	grd.addColorStop(colorStop, 'rgba(' + rgbTwo.r + ', ' + rgbTwo.g + ', ' + rgbTwo.b + ', ' + opacityTwo + ')');
	grd.addColorStop(1.000, 'rgba(' + rgbTwo.r + ', ' + rgbTwo.g + ', ' + rgbTwo.b + ', ' + opacityTwo + ')');

	return grd;
}
