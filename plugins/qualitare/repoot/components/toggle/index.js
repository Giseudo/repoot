import './style.scss'

$$('.rp-toggle').each(function() {
	let $el = $$(this),
		$input = $el.find('.rp-toggle__input')

	$el.on('click', event => {
		$el.toggleClass('is-active')
		$input.prop('checked', $el.hasClass('is-active'))
	})
})
