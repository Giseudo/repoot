<?php namespace Qualitare\Repoot\Components;

use Cms\Classes\ComponentBase;
use ApplicationException;

class Notification extends ComponentBase
{

	public function componentDetails()
	{
		return [
			'name'        => 'Notification',
			'description' => 'Componente de notificação.'
		];
	}

	public function defineProperties()
	{
		return [
			//
		];
	}

	public function onRun()
	{
		//
	}
}
