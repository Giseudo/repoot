<?php namespace Qualitare\Repoot\Components;

use Cms\Classes\ComponentBase;
use ApplicationException;

class Number extends ComponentBase
{

	public function componentDetails()
	{
		return [
			'name'        => 'Number',
			'description' => 'Componente para campo de número.'
		];
	}

	public function defineProperties()
	{
		return [
			'value' => [
				'title' => 'Field value',
				'default' => '0',
				'type' => 'string'
			],
			'name' => [
				'title' => 'Field name',
				'default' => 'text',
				'type' => 'string'
			],
			'label' => [
				'title' => 'Field label',
				'default' => '',
				'type' => 'string'
			],
			'inline' => [
				'title' => 'Is inline',
				'default' => false,
				'type' => 'boolean'
			]
		];
	}

	public function onRun()
	{
		//
	}
}
