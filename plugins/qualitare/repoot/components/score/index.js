import './style.scss'
import ProgressBar from 'progressbar.js'

$$('.rp-score').each(function() {
	let $el = $$(this),
		$progress = $el.find('.rp-score__progress'),
		bar = new ProgressBar.SemiCircle($progress[0], {
			strokeWidth: 8,
			color: '#FFEA82',
			trailWidth: 8,
			trailColor: 'rgb(238, 238, 238)',
			easing: 'easeInOut',
			svgStyle: {
				width: 180
			},
			text: {
				value: '',
				alignToBottom: false
			},
			from: {color: '#FFEA82'},
			to: {color: 'rgb(110, 205, 95)'},
			step: (state, bar) => {
				bar.path.setAttribute('stroke', state.color);
				var value = Math.round(bar.value() * 10);
				if (value === 0) {
					bar.setText('');
				} else {
					bar.setText(value);
				}

				bar.text.style.color = state.color;
			}
		})

	bar.text.style.fontFamily = 'MontSerrat, Arial';
	bar.text.style.fontWeight = '500';
	bar.text.style.fontSize = '110px';
	bar.text.style.margin = '0 0 -10px';
    let value = $el.data('value')
	bar.animate(value/10)
})
