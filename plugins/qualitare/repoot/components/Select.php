<?php namespace Qualitare\Repoot\Components;

use Cms\Classes\ComponentBase;
use ApplicationException;

class Select extends ComponentBase
{

	public function componentDetails()
	{
		return [
			'name'        => 'Select',
			'description' => 'Componente para campo de múltipla/única escolha.'
		];
	}

	public function defineProperties()
	{
		return [
			'label' => [
				'title' => 'Field label',
				'default' => '',
				'type' => 'string'
			],
			'size' => [
				'title' => 'Component size',
				'default' => 'md',
				'type' => 'string'
			],
			'inline' => [
				'title' => 'Is inline',
				'default' => false,
				'type' => 'boolean'
			],
			'options' => [
				'title' => 'The options',
				'default' => [
					0 => 'Selecione uma opção'
				],
				'type' => 'boolean'
			],
			'selected' => [
				'title' => 'Selected options',
				'default' => [0],
				'type' => 'array'
			],
			'multiple' => [
				'title' => 'Has multiple support',
				'default' => false,
				'type' => 'boolean'
			]
		];
	}

	public function onRun()
	{
		//
	}
}
