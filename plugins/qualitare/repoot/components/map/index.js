import './style.scss'

// Wait application load google maps sdk
$$('body').on('maps-init', event => {
	// Google maps object
	let maps = event.detail

	// Map component logic
	$$('.rp-map').each(function() {
		let $el = $$(this),
			markers = [],
			dataMap = JSON.parse($el.data('value')),
			center = JSON.parse($el.data('center')),
			bounds = new maps.LatLngBounds()

		this.map = new maps.Map($el[0], {
			center: {
				lat: +center.lat,
				lng: +center.lng
			},
			zoom: 3.6,
		})

		if (dataMap.length > 0) {
			dataMap.forEach(marker => {
				let icon = '';

				if (marker.color == 'green')
					icon = '/themes/repoot/assets/images/pin-green.png'
				else if (marker.color == 'red')
					icon = '/themes/repoot/assets/images/pin-red.png'
				else
					icon = '/themes/repoot/assets/images/pin-yellow.png'

				let content = `<div class="rp-infowindow rp-infowindow--${marker.color}">${marker.title} <span>${marker.score}</span></div>`,
					infoWindow = new maps.InfoWindow({
						content: content,
						map: this.map
					}),
					length = markers.push(
						new maps.Marker({
							position: {lat: +marker.lat, lng: +marker.lng},
							map: this.map,
							title: marker.title,
							icon: {
								url: icon,
								scaledSize: new google.maps.Size(35, 50),
								origin: new google.maps.Point(0,0),
								anchor: new google.maps.Point(0, 0)
							}
						})
					);

				// Extend boundaries
				bounds.extend(markers[length-1].position)

				// Open infowindow on click event
				markers[length-1].addListener('click', function() {
					infoWindow.open(this.map, markers[length-1]);
				});
			})

			// Center map
			this.map.fitBounds(bounds)
		}

		// Trigger init event
		$el.trigger('init', {
			maps: maps,
			map: this.map
		})
	})
})
