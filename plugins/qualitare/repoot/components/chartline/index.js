import './style.scss'
import Chart from 'chart.js'

$$('.rp-chart-line').each(function() {
	let $el = $$(this),
		$canvas = $el.find('.rp-chart-line__canvas'),
		dataChart = JSON.parse($el.data('value'))

	this.chart = new Chart($canvas, {
		type: 'line',
		data: dataChart,
		options: {
			responsive: true,
			legend: {
				position: 'bottom',
				labels: {
					padding: 20,
					generateLabels: function(chart) {
						let labels = Chart.defaults.global.legend.labels.generateLabels(chart);
						labels[0].fillStyle = 'rgb(118, 193, 95)';
						labels[1].fillStyle = '#ffca5b';
						labels[2].fillStyle = '#f32d4f';

						return labels;
					}
				}
			},
			layout: {
				padding: {
					top: 10,
					right: 10
				}
			},
			scales: {
				xAxes: [{
					gridLines: {
						display: false,
						drawBorder: false,
						drawOnChartArea: false,
						color: 'transparent',
						borderColor: 'rgb(219, 220, 222)',
						drawTicks: false,
						borderDash: [2, 6]
					},
					ticks: {
						padding: 10,
						fontSize: 10,
						fontColor: 'rgb(161, 161, 161)'
					}
				}],
				yAxes: [{
					gridLines: {
						display: true,
						drawBorder: false,
						borderColor: 'rgb(219, 220, 222)',
						drawOnChartArea: true,
						drawTicks: false,
						borderDash: [2, 6]
					},
					ticks: {
						min: 0,
						max: 10,
						stepSize: 5,
						padding: 20,
						fontSize: 10,
						fontColor: 'rgb(161, 161, 161)'
					}
				}]
			}
		}
	})
})

