<?php namespace Qualitare\Repoot\Components;

use Cms\Classes\ComponentBase;
use ApplicationException;

class Plan extends ComponentBase
{

	public function componentDetails()
	{
		return [
			'name'        => 'Plan',
			'description' => ''
		];
	}

	public function defineProperties()
	{
		return [
			'title' => [
				'title' => 'The title',
				'default' => '',
				'type' => 'string'
			],
			'theme' => [
				'title' => 'Theme color',
				'default' => 'green',
				'type' => 'string'
			],
			'features' => [
				'title' => 'The feature list',
				'default' => [],
				'type' => 'array'
			],
			'price' => [
				'title' => 'The price',
				'default' => 0,
				'type' => 'number'
			],
			'cents' => [
				'title' => 'The price cents',
				'default' => 0,
				'type' => 'number'
			],
			'icon' => [
				'title' => 'The icon class',
				'default' => 'plan-basic',
				'type' => 'string'
			]
		];
	}

	public function onRun()
	{
		//
	}
}
