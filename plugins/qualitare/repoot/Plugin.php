<?php namespace Qualitare\Repoot;

/**
 * The plugin.php file (called the plugin initialization script) defines the plugin information class.
 */

use System\Classes\PluginBase;

class Plugin extends PluginBase
{

    public function pluginDetails()
    {
        return [
            'name'        => 'Panel Plugin',
            'description' => '',
            'author'      => 'Qualitare',
            'icon'        => 'icon-leaf'
        ];
    }

    public function registerComponents()
    {
        return [
            '\Qualitare\Repoot\Components\ChartLine' => 'chartline',
            '\Qualitare\Repoot\Components\ChartDoughnut' => 'chartdoughnut',
            '\Qualitare\Repoot\Components\Select' => 'select',
            '\Qualitare\Repoot\Components\SelectDate' => 'selectdate',
            '\Qualitare\Repoot\Components\Text' => 'text',
            '\Qualitare\Repoot\Components\Score' => 'score',
            '\Qualitare\Repoot\Components\Review' => 'review',
            '\Qualitare\Repoot\Components\Notification' => 'notification',
            '\Qualitare\Repoot\Components\Map' => 'map',
            '\Qualitare\Repoot\Components\Filters' => 'filters',
            '\Qualitare\Repoot\Components\Toggle' => 'toggle',
            '\Qualitare\Repoot\Components\Number' => 'number',
            '\Qualitare\Repoot\Components\Plan' => 'plan'
        ];
    }
}
