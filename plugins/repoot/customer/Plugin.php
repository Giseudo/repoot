<?php namespace Repoot\Customer;

use Backend;
use System\Classes\PluginBase;

/**
 * Customer Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Customer',
            'description' => 'No description provided yet...',
            'author'      => 'Repoot',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Repoot\Customer\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'repoot.customer.some_permission' => [
                'tab' => 'Customer',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'customer' => [
                'label'       => 'Customer',
                'url'         => Backend::url('repoot/customer/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['repoot.customer.*'],
                'order'       => 500,
            ],
        ];
    }
}
