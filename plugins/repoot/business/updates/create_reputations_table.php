<?php namespace Repoot\Business\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateReputationsTable extends Migration
{
    public function up()
    {
        Schema::create('repoot_business_reputations', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('repoot_business_reputations');
    }
}
