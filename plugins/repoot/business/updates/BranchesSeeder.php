<?php

namespace Repoot\Business\Updates;

use October\Rain\Database\Updates\Seeder;
use Repoot\Business\Models\Branch;
use Repoot\Business\Models\Company;

class BranchesSeeder extends Seeder {

	public function run() {
		$branch = Branch::create([
			'name' => 'QualiDev',
			'location' => ['lat' => 0, 'lng' => 0],
			'address' => 'Rua Vandick Pinto Filgueiras 613, João Pessoa – PB',
			'company_id' => Company::first()->id,
		]);
	}
}
