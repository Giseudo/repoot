<?php namespace Repoot\Business\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProvidersTable extends Migration
{
	public function up()
	{
		Schema::create('repoot_business_providers', function(Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('name');
			$table->string('slug');
		});
	}

	public function down()
	{
		Schema::dropIfExists('repoot_business_providers');
	}
}
