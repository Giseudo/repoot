<?php

namespace Repoot\Business\Updates;

use October\Rain\Database\Updates\Seeder;
use Repoot\Business\Models\Provider;

class ProvidersSeeder extends Seeder {

	public function run() {
		Provider::create(['name' => 'Tripadvisor', 'slug' => 'tripadvisor']);
		Provider::create(['name' => 'Hoteis.com', 'slug' => 'hoteis']);
		Provider::create(['name' => 'Decolar', 'slug' => 'decolar']);
		Provider::create(['name' => 'Expedia', 'slug' => 'expedia']);
		Provider::create(['name' => 'Booking', 'slug' => 'booking']);
		Provider::create(['name' => 'Trivago', 'slug' => 'trivago']);
	}
}
