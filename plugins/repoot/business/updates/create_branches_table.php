<?php namespace Repoot\Business\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateBranchesTable extends Migration
{
	public function up()
	{
		Schema::create('repoot_business_branches', function(Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('company_id');
			$table->string('name');
			$table->string('location');
			$table->string('address');
			$table->boolean('rival')->default(0);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('repoot_business_branches');
	}
}
