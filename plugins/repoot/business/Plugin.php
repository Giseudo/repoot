<?php namespace Repoot\Business;

use Backend;
use System\Classes\PluginBase;

/**
 * Business Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Business',
            'description' => 'No description provided yet...',
            'author'      => 'Repoot',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Repoot\Business\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'repoot.business.some_permission' => [
                'tab' => 'Business',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'business' => [
                'label'       => 'Business',
                'url'         => Backend::url('repoot/business/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['repoot.business.*'],
                'order'       => 500,
            ],
        ];
    }
}
