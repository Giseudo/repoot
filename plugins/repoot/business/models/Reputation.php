<?php namespace Repoot\Business\Models;

use Model;

/**
 * Reputation Model
 */
class Reputation extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'repoot_business_reputations';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
