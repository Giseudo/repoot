<?php namespace Repoot\Business\Models;

use Model;

/**
 * Branch Model
 */
class Branch extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'repoot_business_branches';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['name', 'location', 'address', 'company_id'];

    /**
     * @var array Jsonable fields
     */
    protected $jsonable = ['location'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
