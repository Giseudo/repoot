<?php namespace Repoot\Store;

use Backend;
use System\Classes\PluginBase;

/**
 * Store Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Store',
            'description' => 'No description provided yet...',
            'author'      => 'Repoot',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Repoot\Store\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'repoot.store.some_permission' => [
                'tab' => 'Store',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'store' => [
                'label'       => 'Store',
                'url'         => Backend::url('repoot/store/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['repoot.store.*'],
                'order'       => 500,
            ],
        ];
    }
}
