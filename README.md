# Repoot - Documentação

## Dependências
- OctoberCMS

## Development
### Inicializando o backend
Será necessário realizar os seguintes passos para inicializar o projeto pela primeira vez:

1. Clone o repositório, acesse seu diretório e em seguida instale as dependências do composer:
`composer install`

2. Em seguida, execute o comando a seguir para realizar os passos de instalação do OctoberCMS:
`php artisan october:install`

3. Siga os passo-a-passo; por praticidade de desenvolvimento, na etapa de conexão com o banco de dados selecione SQLite.

4. Após a finalização de instalação do OctoberCMS, inicialize o servidor com o seguinte comando:
`php artisan serve --host=0.0.0.0 --port=5000`
_PS: Caso deseje utilizar a porta 80, será necessário o uso de `sudo` antes da linha de comando acima._

5. Acesse http://localhost:5000 (ou a porta escolhida) no navegador.

6. Se tudo ocorrer bem, o tema padrão do OctoberCMS será exibido. Acesse o painel administrador em: http://localhost:5000/backend. Use os dados de acesso escolhidos no processo de instalação do framework.

7. Ao entrar no painel administrativo, acesse o menu **Settings**, então **Front-end theme** e selecione o tema **Repoot - Panel**.

### Inicializando o frontend
1. Acesse a raiz do projeto e instale as dependências com:
`npm install`

2. Em seguida copie o arquivo `.env.example` para `.env`

3. Com o seguinte comando, inicie o ambiente de desenvolvimento para o Painel:
`npm run dev:panel`

4. Acesse o endereço: http://localhost:5000 no seu navegador.

### Estrutura do frontend
Utilizamos a estrutura do OctoberCMS. Invés da blade do Laravel, o Twig foi utilizado; os próprios desenvolvedores do framework desencorajam o uso de Blades, pois pode gerar inconsistência na aplicação.

O tema localiza-se em `themes/panel`. Onde possui os seguintes diretórios:

- `layouts` - Arquivos de base para as páginas. Inicializa scripts e estilos.
- `pages` - Cada página representa uma rota da aplicação, opcionalmente diferindo o layout escolhido;
- `partials` - Includes do projeto;
- `stylesheets` - Estilos, temas, variaveis e sistema de grid;
- `assets` - Conteúdo estático da aplicação;
- `dist` - Scripts, estilos e assets compilados são colocados neste diretório;
- `main.js` - Ponto de entrada da aplicação.

### Componentes
Utilizamos o sistema de Componentes do próprio October Framework. Os mesmos localizam-se em `plugins/qualitare/panel/components`, aqui são definidos os componentes, assim como também sua lógica e estilos. Cada componente possui um arquivo `php`, `htm`, `scss` e `js`; cada um possui objetivos distintos.

#### Dependências
**Chart.js** - Biblioteca Javascript de gráficos. Utilizado nos components "Chart Line" e "Chart Doughnut"

**Progressbar.js** - Biblioteca Javascript para barra de progresso. Utilizado no componente "Score".

**jQuery Date Range Picker** - Biblioteca Javascript para date picker. Utilizado no component "Select Date".
