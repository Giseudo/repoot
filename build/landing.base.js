var webpack = require('webpack');
var path = require('path');

// Setup environment variables
require('dotenv').config()

module.exports = {
	entry: {
		'components': './plugins/qualitare/repoot/main.js',
		'landing': './themes/repoot/landing.js'
	},
	resolve: {
		extensions: ['.js'],
		alias: {
			'@': path.join(__dirname, '..', 'themes', 'repoot'),
			'~': path.join(__dirname, '..', 'node_modules'),
			'jQuery': 'jquery/dist/jquery.js',
			'Dom7': 'dom7/dist/dom7.js'
		}
	},
	output: {
		path: path.join(__dirname, '..', 'themes', 'repoot', 'dist'),
		publicPath: '/',
		filename: '[name].bundle.js'
	},
	plugins: [
		// Added jquery global vars
		new webpack.ProvidePlugin({
			$$: 'Dom7',
			Dom7: 'Dom7',
			'window.Dom7': 'Dom7',
			jQuery: 'jQuery',
                        $: 'jQuery',
			
		}),
		new webpack.EnvironmentPlugin({
			NODE_ENV: 'development',
			GOOGLE_MAPS_KEY: null
		})
	],
	module: {
		rules: [
			{
				test: /\.js$/,
				include: path.join(__dirname, '../../plugins/qualitare/repoot/components'),
				exclude: [/node_modules/],
				use: [
					'babel-loader'
				]
			},
			{
				test: /\.scss$/,
				use: [
					'style-loader',
					'css-loader',
					{
						loader: 'sass-loader',
						options: {
							data: "@import './themes/repoot/stylesheets/main.scss';",
							includePaths: [
								require('bourbon-neat').includePaths,
							]
						}
					}
				]
			},
			{
				test: /\.(woff|woff2|eot|ttf|svg)$/,
				exclude: /node_modules/,
				use: [
					{
						loader: 'url-loader',
						options: {
							limit: '1024',
							publicPath: '/themes/repoot/dist',
							name: 'fonts/[name].[ext]'
						}
					}
				]
			},
			{
				test: /\.(jpg|jpeg|gif|png)$/,
				exclude: /node_modules/,
				use: [
					{
						loader: 'url-loader',
						options: {
							limit: '1024',
							publicPath: '/themes/repoot/dist',
							name: 'images/[name].[ext]'
						}
					}
				]
			}
		]
	}
};
