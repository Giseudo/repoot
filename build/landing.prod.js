var webpack = require('webpack');
var path = require('path');
var webpackMerge   = require('webpack-merge');
var webpackBase = require('./panel.base');

// Plugins
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = webpackMerge
	.smartStrategy({ 'module.rules.use': 'prepend' })(webpackBase, {
		devtool: 'cheap-module-source-map',
		plugins: [
			// Extract CSS to file
			new ExtractTextPlugin({
				filename: 'css/[name].bundle.css',
				disable: false,
				allChunks: true
			}),
			new webpack.LoaderOptionsPlugin({
				minimize: true,
				debug: false
			}),
			new webpack.optimize.CommonsChunkPlugin({
				minChunks: 2,
				async: true
			}),
			new UglifyJsPlugin({
				uglifyOptions: {
					mangle: {
						safari10: true
					}
				}
			})
		],
		module: {
			rules: [
				{
					test: /\.scss$/,
					use: ExtractTextPlugin.extract({
						fallback: 'style-loader',
						use: [
							'css-loader',
							{
								loader: 'sass-loader',
								options: {
									data: "@import './themes/repoot/stylesheets/main.scss';",
									includePaths: [
										require('bourbon-neat').includePaths,
									]
								}
							}
						]
					})
				}
			]
		}
	}
)
