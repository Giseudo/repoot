import './login.scss'
import Swiper from 'swiper/dist/js/swiper'

$$('.rp-auth-slider').each(function() {
	let $el = $$(this),
		swiper = new Swiper($el, {
			pagination: {
				el: '.swiper-pagination',
				clickable: true
			}
		})
})
