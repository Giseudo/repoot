import './register.scss'
import Swiper from 'swiper'

$$('.rp-register').each(function() {
	let $el = $$(this),
		$plans = $el.find('.rp-plan'),
		$current = $el.find('.rp-register-step__current'),
		$total = $el.find('.rp-register-step__total'),
		swiper = new Swiper($el.find('.swiper-container'), {
			speed: 600,
			on: {
				slideChange: () => {
					$current.html(swiper.activeIndex + 1)
				}
			}
		})

	$plans.each(function() {
		let $plan = $$(this),
			$confirm = $plan.find('.rp-button')

		$confirm.on('click', event => {
			swiper.slideNext()
			$('body, html').animate({ scrollTop: 0 }, 600)
		})
	})

	// Disable drag interaction
	swiper.allowTouchMove = false

	// Update total steps
	$total.html(swiper.slides.length)
	$current.html(swiper.activeIndex + 1)
})
