import './account.scss'

$$('.rp-account').each(function() {
	let $el = $$(this),
		$tabs = $el.find('.rp-account__tab'),
		hash = window.location.hash

	$tabs.each(function() {
		let $tab = $$(this)

		$tab.on('click', function() {
			let $target = $el.find($tab.data('target'))

			// Remove active class
			$el.find('.rp-account__tab-content').removeClass('is-active')
			$tabs.removeClass('is-active')
			$tabs.removeClass('rp-button--outline')

			// Add active class
			$tab.addClass('is-active')
			$tab.addClass('rp-button--outline')
			$target.addClass('is-active')
		})
	})

	if (hash) {
		let $tab = $el.find(`[data-target="${hash}"]`),
			$target = $el.find(hash)

		// Remove active class
		$el.find('.rp-account__tab-content').removeClass('is-active')
		$tabs.removeClass('is-active')
		$tabs.removeClass('rp-button--outline')

		// Add active class
		$tab.addClass('is-active')
		$tab.addClass('rp-button--outline')
		$target.addClass('is-active')
	}
})
