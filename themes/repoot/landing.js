import './partials/landing'
import './pages'
import './stylesheets/default.scss'

// Modal behavior
$$('.rp-modal').each(function() {
	let $el = $$(this)

	$el.children(':first-child').on('click', event => {
		event.stopPropagation()
	})

	$el.on('click', event => {
		this.close()
	})

	this.open = function() {
		document.body.style.overflow = 'hidden';
		$el.addClass('is-active')
	}

	this.close = function() {
		document.body.style.overflow = null;
		$el.removeClass('is-active')
	}
})
