import './landing-nav.scss'

$$('.rp-landing-nav').each(function() {
	let $el = $$(this),
		$links = $el.find('.rp-landing-nav__menu a')

	$links.each(function() {
		let $link = $$(this)

		$link.on('click', event => {
			let $target = $$($link.attr('href')),
				scroll = $$('html').scrollTop(),
				header = $$('.rp-landing-header').height()

			if ($target.length > 0) {
				$('html').animate({
					scrollTop: $target.offset().top + scroll - header
				})
				event.preventDefault()
			}
		})
	})
})
