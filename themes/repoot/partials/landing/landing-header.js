import './landing-header.scss'

$$('.rp-landing-header').each(function() {
	let $el = $$(this),
		$menu = $el.find('.rp-landing-header__menu'),
		$sidenav = $el.find('.rp-landing-sidenav')

	$(window).on('scroll', event => {
		var scroll = document.scrollingElement.scrollTop

		if (scroll <= 100)
			$el.removeClass('is-fixed')
		else
			$el.addClass('is-fixed')
	})

	$menu.on('click', event => {
		$sidenav.toggleClass('is-active')
		event.preventDefault()
	})
})
