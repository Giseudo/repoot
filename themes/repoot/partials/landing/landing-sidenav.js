import './landing-sidenav.scss'

$$('.rp-landing-sidenav').each(function() {
	let $el = $$(this),
		$links = $el.find('.rp-landing-sidenav__nav a')

	$links.each(function() {
		let $link = $$(this)

		$link.on('click', event => {
			let $target = $$($link.attr('href')),
				scroll = $$('html').scrollTop()

			if ($target.length > 0) {
				$('html').animate({
					scrollTop: $target.offset().top + scroll
				})
				$el.removeClass('is-active')
				event.preventDefault()
			}
		})
	})
})
