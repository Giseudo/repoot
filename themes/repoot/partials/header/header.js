import './header.scss'

$$('.rp-header').each(function() {
	let $el = $$(this),
		$actions = $el.find('.rp-header__action'),
		$menu = $el.find('.rp-header__menu'),
		$sidenav = $el.find('.rp-header-sidenav')

	$actions.each(function() {
		let $action = $$(this),
			$dropdown = $action.parent().find('.rp-header__dropdown')

		$action.on('click', event => {
			$el.find('.rp-header__dropdown').removeClass('is-active')
			$dropdown.addClass('is-active')
			$sidenav.removeClass('is-active')

			event.stopPropagation()
		})

		$dropdown.on('click', event => {
			$dropdown.addClass('is-active')

			event.stopPropagation()
		})
	})

	$$('body').on('click', event => {
		$el.find('.rp-header__dropdown').removeClass('is-active')
	})

	$menu.on('click', event => {
		$sidenav.toggleClass('is-active')
		$el.find('.rp-header__dropdown').removeClass('is-active')

		event.stopPropagation()
	})

	$sidenav.on('click', event => {
		event.stopPropagation()
	})

	$$('body').on('click', event => {
		$sidenav.removeClass('is-active')
	})
})
