import './account-profile.scss'

$$('.rp-account-profile').each(function() {
	let $el = $$(this),
		$add = $el.find('.rp-account-profile__add, .rp-account-profile__edit'),
		$modal = $el.find('.rp-modal'),
		$cancel = $el.find('.rp-store-form__cancel')

	$add.on('click', event => {
		$modal[0].open()
		event.preventDefault()
	})

	$cancel.on('click', event => {
		$modal[0].close()
		event.preventDefault()
	})
})
