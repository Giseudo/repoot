import './account-rival.scss'

$$('.rp-account-rival').each(function() {
	let $el = $$(this),
		$add = $el.find('.rp-account-rival__add, .rp-account-rival__edit'),
		$modal = $el.find('.rp-modal'),
		$cancel = $el.find('.rp-store-form__cancel')

	$add.on('click', event => {
		$modal[0].open()
		event.preventDefault()
	})

	$cancel.on('click', event => {
		$modal[0].close()
		event.preventDefault()
	})
})
