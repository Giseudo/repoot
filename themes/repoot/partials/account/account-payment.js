import './account-payment.scss'

$$('.rp-account-payment').each(function() {
	let $el = $$(this),
		$plan = $el.find('.rp-account-payment__plan'),
		$items = $el.find('.rp-account-payment__item'),
		$modal = $el.find('.rp-account-payment__modal')

	$plan.on('click', event => {
		$modal[0].open()
		event.preventDefault()
	})

	$items.each(function() {
		let $item = $(this),
			$toggle = $item.find('.rp-account-payment__toggle'),
			$accordion = $item.find('.rp-account-payment__accordion')

		$toggle.on('click', event => {
			if ($accordion.css('display') == 'none')
				$toggle.html('Ocultar Detalhes')
			else
				$toggle.html('Ver Detalhes')

			$accordion.slideToggle()
		})
	})
})
