import './store-form.scss'

$$('.rp-store-form').each(function() {
	let $el = $$(this),
		$tabs = $el.find('.rp-store-form__tab'),
		$map = $el.find('.rp-map'),
		$input = $el.find('input[name="location"]')

	$tabs.each(function() {
		let $tab = $$(this)

		$tab.on('click', function() {
			let $target = $el.find($tab.data('target'))

			// Remove active class
			$el.find('.rp-store-form__tab-content').removeClass('is-active')
			$tabs.removeClass('is-active')

			// Add active class
			$tab.addClass('is-active')
			$target.addClass('is-active')
		})
	})

	// Wait google maps load
	$map.on('init', event => {
		let maps = event.detail.maps,
			map = event.detail.map,
			autocomplete = new maps.places.Autocomplete($input[0])

		autocomplete.bindTo('bounds', map)

		var marker = new maps.Marker({
			map: map,
			anchorPoint: new maps.Point(0, -29)
		});

		autocomplete.addListener('place_changed', function() {
			let place = autocomplete.getPlace(),
				address = ''

			marker.setVisible(false)

			// If the place has a geometry, then present it on a map.
			if (place.geometry.viewport) {
				map.fitBounds(place.geometry.viewport)
			} else {
				map.setCenter(place.geometry.location)
				map.setZoom(17)
			}

			marker.setIcon(({
				url: place.icon,
				size: new maps.Size(71, 71),
				origin: new maps.Point(0, 0),
				anchor: new maps.Point(17, 34),
				scaledSize: new maps.Size(35, 35)
			}))

			marker.setPosition(place.geometry.location);
			marker.setVisible(true);

			if (place.address_components) {
				address = [
					(place.address_components[0] && place.address_components[0].short_name || ''),
					(place.address_components[1] && place.address_components[1].short_name || ''),
					(place.address_components[2] && place.address_components[2].short_name || '')
				].join(' ');
			}
		})
	})
})
