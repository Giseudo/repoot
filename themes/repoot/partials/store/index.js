import './store-form'

$$('.rp-store-form').each(function() {
	let $el = $$(this),
		$tabs = $el.find('.rp-store-form__tab')

	$tabs.each(function() {
		let $tab = $$(this)

		$tab.on('click', function() {
			let $target = $el.find($tab.data('target'))

			// Remove active class
			$el.find('.rp-store-form__tab-content').removeClass('is-active')
			$tabs.removeClass('is-active')
			$tabs.removeClass('rp-button--outline')

			// Add active class
			$tab.addClass('is-active')
			$tab.addClass('rp-button--outline')
			$target.addClass('is-active')
		})
	})
})
