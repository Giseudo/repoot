import './partials'
import './pages'
import './stylesheets/default.scss'
import loadGoogleMapsApi from 'load-google-maps-api';

// Add delay to load google maps
setTimeout(() => {
	loadGoogleMapsApi({
		key: process.env.GOOGLE_MAPS_KEY,
		libraries: [
			'places'
		]
	})
		.then(maps => {
			$$('body').trigger('maps-init', maps)
		})
}, 4000)

// Modal behavior
$$('.rp-modal').each(function() {
	let $el = $$(this)

	$el.children(':first-child').on('click', event => {
		event.stopPropagation()
	})

	$el.on('click', event => {
		this.close()
	})

	this.open = function() {
		document.body.style.overflow = 'hidden';
		$el.addClass('is-active')
	}

	this.close = function() {
		document.body.style.overflow = null;
		$el.removeClass('is-active')
	}
})
